import {gsap, Linear} from "gsap"
const activated = [
    gsap, gsap, Linear
]
export default async  (context, inject) => {
  const OptionsObject = {
    gsap, Linear
  }
  Object.keys(OptionsObject).forEach(key => gsap[key] = OptionsObject[key] )
  inject('gsap', gsap)
}
