import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _cad2b19a = () => interopDefault(import('../pages/about.vue' /* webpackChunkName: "pages/about" */))
const _52067ac6 = () => interopDefault(import('../pages/contact.vue' /* webpackChunkName: "pages/contact" */))
const _1e60adbc = () => interopDefault(import('../pages/faq.vue' /* webpackChunkName: "pages/faq" */))
const _8b3842fa = () => interopDefault(import('../pages/therapists.vue' /* webpackChunkName: "pages/therapists" */))
const _8b62fc10 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/about",
    component: _cad2b19a,
    name: "about"
  }, {
    path: "/contact",
    component: _52067ac6,
    name: "contact"
  }, {
    path: "/faq",
    component: _1e60adbc,
    name: "faq"
  }, {
    path: "/therapists",
    component: _8b3842fa,
    name: "therapists"
  }, {
    path: "/",
    component: _8b62fc10,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
