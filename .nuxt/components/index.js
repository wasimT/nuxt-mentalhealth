export { default as Counsellors } from '../../components/Counsellors.vue'
export { default as Footer } from '../../components/Footer.vue'
export { default as Navbar } from '../../components/Navbar.vue'
export { default as NewsletterForm } from '../../components/NewsletterForm.vue'
export { default as PageLoader } from '../../components/PageLoader.vue'
export { default as Section2 } from '../../components/Section2.vue'
export { default as Services } from '../../components/Services.vue'
export { default as Testimonials } from '../../components/Testimonials.vue'

export const LazyCounsellors = import('../../components/Counsellors.vue' /* webpackChunkName: "components/Counsellors" */).then(c => c.default || c)
export const LazyFooter = import('../../components/Footer.vue' /* webpackChunkName: "components/Footer" */).then(c => c.default || c)
export const LazyNavbar = import('../../components/Navbar.vue' /* webpackChunkName: "components/Navbar" */).then(c => c.default || c)
export const LazyNewsletterForm = import('../../components/NewsletterForm.vue' /* webpackChunkName: "components/NewsletterForm" */).then(c => c.default || c)
export const LazyPageLoader = import('../../components/PageLoader.vue' /* webpackChunkName: "components/PageLoader" */).then(c => c.default || c)
export const LazySection2 = import('../../components/Section2.vue' /* webpackChunkName: "components/Section2" */).then(c => c.default || c)
export const LazyServices = import('../../components/Services.vue' /* webpackChunkName: "components/Services" */).then(c => c.default || c)
export const LazyTestimonials = import('../../components/Testimonials.vue' /* webpackChunkName: "components/Testimonials" */).then(c => c.default || c)
