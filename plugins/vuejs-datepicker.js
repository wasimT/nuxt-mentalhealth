import Vue from "vue";
import Datepicker from "vuejs-datepicker";

import '~/assets/css/date-picker.css';

// Vue.use(DatePicker)
Vue.component("date-picker", Datepicker);
