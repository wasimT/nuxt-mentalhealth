export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: "static",

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: "Confidous",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content:
          "Confidous is a mental health startup educating and helping people about mindfulness and other mind related stuff.",
      },
      {
        hid: "keywords",
        name: "keywords",
        content:
          "mental, health, wellness, mindfulness, therapy, awareness",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
    script: [
      {
        hid: "GTM",
        src: "https://www.googletagmanager.com/gtag/js?id=G-07Q66BV7WM",
        async: true,
      },
      { hid: "GTA", src: "/gta.js", async: true },
    ],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [{ src: "~/assets/css/fonts.css", land: "css" }],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: "~/plugins/vuejs-datepicker", ssr: false },

    { src: "~/plugins/vuelidate" },
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    "@nuxtjs/tailwindcss",
    "nuxt-gsap",
    
  ],
  // googleAnalytics: {
  //   id: 'UA-192645459-1',
  //   debug: {
  //     enabled: true,
  //     sendHitTask: true
  //   },
  // },

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: ["nuxt-gsap", "@nuxtjs/axios"],
  // "google-gtag": {
  //   id: "G-07Q66BV7WM",
  //   config: {
  //     anonymize_ip: true, // anonymize IP
  //     send_page_view: false, // might be necessary to avoid duplicated page track on page reload
  //   },
  //   debug: true, // enable to track in dev mode
  // },

  axios: {
    baseURL: "http://localhost:4000/api/",
    // baseURL: 'https://confidous.herokuapp.com/api/'
  },

  publicRuntimeConfig: {
    axios: {
      browserBaseURL: "http://localhost:4000/api/",
      // browserBaseURL: 'https://confidous.herokuapp.com/api/'
    },
  },

  privateRuntimeConfig: {
    axios: {
      browserBaseURL: "http://localhost:3000/api/",
    },
  },

  router: {
    prefetchLinks: true,
    linkPrefetchedClass: 'nuxt-link-prefetched'
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    transpile: ["gsap"],
    extractCSS: true,
  },
};
