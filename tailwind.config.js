module.exports = {
  purge: [
    './components/**/*.{vue,js}',
     './layouts/**/*.vue',
     './pages/**/*.vue',
     './plugins/**/*.{js,ts}',
     './nuxt.config.{js,ts}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
     
      colors: {
        primary: '#106C5F',
        secondary: '#E3BA26',
        accent: '#F16A29',
        dark: '#162C27',
        light: '#D1E4E0',
        mildwhite: '#CAE3D5'
      },
      fontSize: {
        '1.5xl': '1.35rem',
      },
      screens: {
        xs: '520px'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
